import React, { useState } from "react";
import { connect } from "react-redux";

function ProfilePage(props) {
  const [editedName, setName] = useState();
  const [editedSurname, setSurname] = useState();
  const [editedCard, setCard] = useState();

  const handleEdit = () => {
     let user = {
         name: editedName ? editedName: props.profile.name,
         surname: editedSurname ? editedSurname: props.profile.surname,
         card: editedCard ? editedCard: props.profile.card,
     }
     
     props.editUser(user)

     setName('')
     setSurname('')
     setCard('')

  }

  return (

    <div>
        <h1>Данные пользователя</h1>
        <div className="post">
        <div className="inputForm">
            <strong>Имя: <br/>{props.profile.name}</strong><br/>
            <strong>Фамилия: <br/>{props.profile.surname}</strong><br/>
            <strong>Номер карты: <br/>{props.profile.card}</strong>
        </div>
        Имя <input className="inputForm" value={editedName} onChange={(e) => setName(e.target.value)}/>
        фамилия <input className="inputForm" value={editedSurname} onChange={(e) => setSurname(e.target.value)}/>
        Номер карты <input className="inputForm" value={editedCard} onChange={(e) => setCard(e.target.value)}/>
        <button onClick = {handleEdit}>Изменить</button>
        </div>
    </div>
  )
}

const mapStateToProps = (state) => ({
    profile: state.profile
});

const mapDispatchToProps = (dispatch) => ({
    editUser: (data) => {dispatch({type: 'EDIT_USER', data})},
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProfilePage);