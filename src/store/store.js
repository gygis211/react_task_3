import { createStore, combineReducers } from "redux";
import homeReducer from "../reducers/home.reducer";
import newsListReducer from "../reducers/news.reducer";
import profileReducer from "../reducers/profile.reducer"

export const rootReducer = combineReducers({
  home: homeReducer,
  profile: profileReducer,
  news: newsListReducer
});

export const store = createStore(rootReducer);
