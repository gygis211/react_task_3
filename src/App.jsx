import React from "react";
import NavigationMenu from "./components/NavigationMenu.jsx";
import HomePage from "./components/HomePage";
import Admin from './components/AdminPage';
import NewsPage from "./components/NewsPage";
import Profile from "./components/ProfilePage";
import { BrowserRouter, Route } from "react-router-dom";
import { Provider } from "react-redux";
import { store } from "./store/store";

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <NavigationMenu />
        <Route path="/home" component={HomePage} />
        <Route path="/admin" component={Admin} />
        <Route path="/news" component={NewsPage} />
        <Route path="/profile" component={Profile} />
      </BrowserRouter>
    </Provider>
  );
}

export default App;
